import sys
import setuptools

try:
    import nsblock
except ImportError:
    print("error: nsblock requires Python 3.8 or greater.")
    sys.exit(1)

LONG_DESC = open('README.md').read()
DESCRIPTION = "NSBlock - simple cross-platform"
"python script for generating host files"
"to block ads, trackers and other nasty things."

setuptools.setup(
    name="nsblock",
    version="0.0.6",
    author="Vadim Trifonov",
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    long_description=LONG_DESC,
    keywords="adblock nsblock block-ad",
    license="MIT",
    url="https://gitlab.com/muffinsupp1y/nsblock",
    entry_points={"console_scripts": ["nsblock=nsblock.__main__:main"]},
    packages=['nsblock'],
    python_requires=">=3.8",
    zip_safe=False,
)
